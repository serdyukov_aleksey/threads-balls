import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class MainPanel extends JPanel {

    private List<Ball> balls = new ArrayList<>() ;
    private Random rdn = new Random();
    Ball kickingBall;
    private Set<Thread> setOfThread;

    MainPanel() {
    }

    boolean isPointInBall (int x, int y){
        int radius;
        for (Ball ball:balls) {
            radius = ball.getSize() / 2;
            int ballCenterX = ball.getX() + radius;
            int ballCenterY = ball.getY() + radius;
            Point p = new Point(ballCenterX, ballCenterY);
            double d = p.distance(x,y);
            if (d < radius) {
                kickingBall=ball;
                return true;
            }
        }
        return false;
    }

    void kickBall (Ball ball){
        setOfThread = Thread.getAllStackTraces().keySet();
        for(Thread thread : setOfThread){
            if(thread.getName().equals(ball.getUid())){
                thread.interrupt();
            }
        }
        balls.remove(ball);
    }

    void addBall(int x, int y){
        Ball b = new Ball(
                x, y,
                new Color(rdn.nextInt(255), rdn.nextInt(255), rdn.nextInt(255)),
                rdn.nextInt(40)+30,
                new Rectangle(getWidth(), getHeight()),
                5 + rdn.nextInt(40)) {

            @Override
            public void doDraw() {
                MainPanel.this.repaint();
            }
        };

        this.balls.add(b) ;
        Thread thread = new Thread(b);
        thread.setName(b.getUid().toString());
        thread.start();
        MainPanel.this.repaint();
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (Ball ball : balls) {
            g.setColor(ball.getColor());
            g.fillOval(ball.getX(),ball.getY(), ball.getSize(), ball.getSize());
        }

    }
}