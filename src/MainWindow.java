import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainWindow extends JFrame {

    private static final int WIDTH = 900;
    private static final int HEIGTH = 600;

    private MainPanel mainPanel;

    public static void main(String[] args) {
                    MainWindow frame = new MainWindow();
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);

    }


    private MainWindow() {
        setTitle("Threads Balls");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, WIDTH, HEIGTH);

        mainPanel = new MainPanel();
        getContentPane().add(mainPanel,BorderLayout.CENTER);

        JPanel panel = new JPanel();
        getContentPane().add(panel,BorderLayout.SOUTH);

        mainPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (!mainPanel.isPointInBall(e.getX(), e.getY())) {
                    mainPanel.addBall(e.getX(), e.getY());
                }
                else {
                    mainPanel.kickBall(mainPanel.kickingBall);
                }
            }
        });
    }



}