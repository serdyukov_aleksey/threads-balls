import java.awt.*;
import java.rmi.server.UID;

public abstract class Ball implements Runnable  {
    private int x,y;
    private Color color;
    private int size ;
    private Rectangle rect;
    private boolean incX,incY;
    private int speed ;
    private UID uid;



    Ball(int x, int y, Color color, int size, Rectangle container, int speed) {
        super();
        this.x = x;
        this.y = y;
        this.size = size;
        this.rect = container;
        this.color = color;
        this.speed = speed ;
        this.uid=new UID();
    }


    private void move(){
        checkTouch();
        this.x = incX ? this.x + 1 : this .x -1 ;
        this.y = incY ? this.y + 1 : this .y -1 ;
        this.doDraw();
    }


    int getX() {
        return x;
    }


    int getY() {
        return y;
    }


    int getSize() {
        return size;
    }

    UID getUid() {
        return uid;
    }

    public abstract void doDraw();

    private void checkTouch() {

        if(this.x <= 0){
            this.incX = true;
        } else if((this.x + size) >= this.rect.width){
            this.incX = false;
        }

        if(this.y <= 0){
            this.incY = true;
        } else if((this.y + size) >= this.rect.height){
            this.incY = false;
        }

    }


    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            move();
            try {
                Thread.sleep(speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    Color getColor(){
        return this.color;
    }


}